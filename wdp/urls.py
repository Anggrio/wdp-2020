from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('description', views.description, name='description'),
    path('background', views.background, name='background'),
    path('contact', views.contact, name='contact'),
    path('challenge', views.challenge, name='challenge'),
    path('friends', views.friends, name='friends'),
    path('forms', views.forms, name='forms'),
]