from django.forms import ModelForm, TextInput, Select
from .models import Friend

class FriendsForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'hobby', 'favorite_food_or_drink', 'batch_year']
        widgets = {
            'name': TextInput(attrs={'class': 'form-control'}),
            'hobby': TextInput(attrs={'class': 'form-control'}),
            'favorite_food_or_drink': TextInput(attrs={'class': 'form-control'}),
            'batch_year': Select(attrs={'class': 'form-control'})
        }

