from django.contrib import admin
from .models import Year, Friend

# Register your models here.
admin.site.register(Year)
admin.site.register(Friend)