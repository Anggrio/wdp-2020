from django.db import models

# Create your models here.
class Year(models.Model):
    year = models.CharField(max_length=20)

    def __str__(self):
        return self.year

class Friend(models.Model):
    name = models.CharField(max_length=40)
    hobby = models.CharField(max_length=30)
    favorite_food_or_drink = models.CharField(max_length=30)
    batch_year = models.ForeignKey(Year, on_delete = models.CASCADE)

    def __str__(self):
        return self.name
