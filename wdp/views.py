from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Friend
from .forms import FriendsForm

# Create your views here.
def index(request):
    return render(request, 'index.html')

def description(request):
    return render(request, 'description.html')

def background(request):
    return render(request, 'background.html')

def contact(request):
    return render(request, 'contact.html')

def challenge(request):
    return render(request, 'challenge.html')

def friends(request):
    friend = Friend.objects.all()
    return render(request, 'friends.html', {'friends': friend})

def forms(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FriendsForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('friends')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FriendsForm()

    return render(request, 'forms.html', {'form': form})